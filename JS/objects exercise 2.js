/* JAVASCRIPT OBJECTS */
// Complete the following in a new js file named objects.js
"use strict";
/**
 * TODO: Create an object called movie. Include four properties
 *  Console log each property.
 */
let movie1 = ["name", "Genre", "Rating", "review"]
console.log (movie1[0]);
console.log (movie1[1]);
console.log (movie1[2]);
console.log (movie1[3]);

/**
 * TODO: Create an object called movie include four properties of title, protagonist, antagonist, genre
 *  console log each property.
 */
let movie =["title", "protagonist", "antagonist","genre"]
console.log (movie);

/**
 * TODO: Create a pet/companion object include the following properties:
 *  string name, number age, string species, number valueInDollars,
 *  array nickNames, vaccinations: date, type, description.
 */
let petCompanion = {
    name: 'toto',
    age: 10,
    species: 'laboradoodle',
    valueInDollars: 0,
    nicknames: ["dingo", ],
    vaccinations: [
        {
            date: "1/2/2020",
            type: "Chihuahua",
            description: "fat"
        },{
            date: "1/2/2020",
            type: "Chihuahua",
            description: "fat"
        }
    ]
}
console.log(petCompanion)

// let newVaccinations = [
//     {
//         date: "1/2/2020",
//         type: "Shitsu",
//         description: "fat"
//     },
//     {
//         date: "1/2/2020",
//         type: "Husky",
//         description: "fat"
//     }
// ]
//
// newVaccinations.forEach(function (element) {
//     petCompanion.vaccinations.push(element);
// })
//
//


/**
 * TODO: Create an array of 4 objects called listOfMovies that include five properties: title, releaseDate, director, genre, and myReview

 *  One function named myReview() for each movie

 *  Loop through the array to display and console log the 'Release Date: month / date / year'.

 *  Loop through the array to display and console log the 'Title, ReleaseDate, Director, Genre, myReview'

 *  Loop through the array to display and console log the 'Director: FirstName LastName'
 */
let listOfMovies = [
    {
        title: 'Pulp Fiction',
        releaseDate: {
            month: 'October',
            date: 14,
            year:1994,
        },
        Director: {
            firstName: 'Quenton',
            lastName: 'Tarantino',
        },
        Genre: ['Action', 'Adventure', 'kindaRapey'],
        myReview: function (){
            console.log('This is Pulp Fiction John Travolta is Vincent Vega 4/4 stars!!')
        }
    },

]

// for(var i  = 0; i < listOfMovies.length; i++) {
//     let releaseDate = listOfMovies[i].releaseDate;
//     console.log(releaseDate.month)
//     console.log(releaseDate.date)
//     console.log(releaseDate.year)
// }

listOfMovies.forEach(function(movie) {
    let release = movie.releaseDate;
    console.log(release.date, release.month, release.year)
});

for (var i = 0; i < listOfMovies.length; i++){
    console.log(listOfMovies[i])
}

listOfMovies.forEach(function (movie){
    let names = movie.Director;
    console.log(names.firstName, names.lastName)
})

// listOfMovies[0]
//EX
const movies1 = [
    {
        title: 'Batman',
        releaseDate: {
            month: 'June',
            date: 23,
            year: 1989
        },
        director: {
            firstName: 'Tim',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'],
        myReview: function () {
            console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
        }
    },
];

/**ADDITIONAL OBJECTS PRACTICE
 *
 1. Create an object named 'dreamCar' with as many properties that
 describes your 'Dream Car/Truck/etc.'
 2. Create an array of objects named 'videoGames'. Each object should have a
 title, year of released, develop company, and creator: {firstName:, lastName}
 - extra challenge: create a property named 'creatorFullName' that has a function
 */
const dreamCar = {
    make: 'ram',
    model:'3500',
    year: 2006,
    color: 'green',
    engine:{
        fuelType: 'Diesel',
        displacement: 6.7,
        },
    accessories:['Air Conditioning', 'Bluetooth', 'Backup Camera', 'Alarm', 'gps','nav',],
    afterMarketAdditions: ['Air Bags', 'Pro Heat', ],
}
