/* JAVASCRIPT ARRAYS */
// Complete the following in a new js file named arrays.js


// "use strict";


/** TODO:
 *  x- Create a new EMPTY array named sports.**/
/** x- Push into this array 5 sports**/
/** x- Sort the array.**/
/** x- Reverse the sort that you did on the array.**/
/** x- Push 3 more sports into the array **/
/** x- Locate your favorite sport in the array and Log the index. **/
/** x-Remove the 4th element in the array **/
/** x- Remove the first sport in the array **/
/** Turn your array into a string  **/
/**
 */
let sports = [];
    sports.push("hockey", "curling", "baseball", "boxing", "wrestling")
    console.log(sports.sort());
    sports.reverse();
    console.log(sports);
    sports.push("karate", "basketball", "sumo");
    console.log(sports)
    let sportsIndex = sports.indexOf('baseball');
    console.log(sportsIndex);
    let splice = sports.splice(3,1)
    console.log(splice);
    sports.shift();
    console.log(sports);
    let newSports = sports.join(",");
    console.log(newSports);









 /*
 /** * TODO: Create an array to hold your top singers/bands/etc.
 /**  const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
 /** 1. console log 'Van Halen'
 /** 2. add 'Fleetwood Mac' to the beginning of the array
 /** 3. add 'Journey' to the end of the array
*/

let bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
    console.log(bands[2]);
    bands.unshift('Fleetwood Mac');
    console.log(bands);
    bands.push('Journey');
    console.log(bands)





/** x- Create a new array named cohort_name that holds the names of the people in your class**/

let cohort_name =['Shaun', 'Justin', 'Stephen', 'Leslie']


/**  x- Log out each individual name from the array by accessing the index. **/
for (let i = 0 ; i <= cohort_name.length; i++)
{
    console.log(cohort_name[i])
}

/**  x- Using a for loop, Log out every item in the COHORT_NAME array **/
for ( let log=0; log < cohort_name.length; log++ )
    console.log( log )

/**  x- Refactor your code using a forEach Function **/
cohort_name.forEach(function(name)
{
    console.log(name);
});


/** Using a for loop. Remove all instance of 'CodeBound' in the array **/
let arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];
// let remove1 = arrays1.splice(1,1,);
// let remove2 = arrays1.splice(3,1)
// console.log(remove2,remove1)
for (let i =0; i < arrays1.length; i++){
    if (arrays1[i] === "CodeBound") {
    arrays1.splice(i, 1)
    }
}
console.log(arrays1)
/**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/
let numbers1 = [3, '12', 55, 9, '0', 99];
let sum = 0;
numbers1.forEach(function (element){
  sum += parseFloat(element);

})
console.log(sum)


/**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/
let numbers2 = [7, 10, 22, 87, 30];
numbers2.forEach(function (element){
    console.log(element * 5);
})

/** Create a function named removeFirstLast where you remove the first and last character of a string.**/


// Bonus
/** Write a function named phoneNum that accepts an array of 10 integers (between 0 and 9). This will return a string of those numbers in a
 *  phone number format.
 *  phoneNum([7,0,8,9,3,2,0,1,2,3])=> returns "(708) 932-0123"
 */

/** Create a function that will take in an integer as an argument and Returns "Even" for even numbers and returns "Odd" for odd numbers **/

/** Create a function named reverseString that will take in a string and reverse it. There are many ways to do this using JavaScript built-in
 * methods. For this exercise use a loop.
 */


/**Create a function named converNum that will convert a group of numbers to a reversed array of numbers**
 * 386543 => [3,4,5,6,8,3]
 */



