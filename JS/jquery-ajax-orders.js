
// BASIC REQUEST
// $.ajax("/some-url")
// default to GET request
//AJAX OPTIONS/PROPERTIES
// $.ajax( "/some-url", {
//         type: "POST",
//         data: {
//             name: "John",
//             location: "Calgary"
//         }
//     }
// )
/*
 Most common options
 type - the type of request to send to the server
 "GET", "POST", "DELETE", "PUT"
 data - data we want to include in the request
 dataType - the type of data we expect from the server
 url - alternate way of specifying the url
 headers - an object of which key value pairs represent to send along with the request
 HANDLING RESPONSES
 $.ajax("/some/path/to/a/file.json").done(function(data, status, jqXhr) {
    // data - the body of the response from the server
    // status - a string indicating the status of the request
    // jqXhr - a jQuery object that represents the ajax request
 }).fail(function(){
 }).success(function(){
 }).always(function(){
 })
 // More methods for handling responses
/*
 .fail()
 .always()
*/
let ajaxRequest = $.ajax("data/orders.json");
ajaxRequest.done(function(data) {
    let dataHTML = renderOrders(data)
    console.log(dataHTML)
    $('#orders').html(dataHTML)
})
function renderOrders(orders) {
    let orderHTML = '';
    orders.forEach(function (order) {
        orderHTML += '<section>'
        orderHTML += '<dl>'
        orderHTML += '<dt>Ordered Item</dt>'
        orderHTML += '<br>'
        orderHTML += `<dd>${order.item}</dd>`
        orderHTML += '<br>'
        orderHTML += '<dt>Ordered by</dt>'
        orderHTML += `<dd>${order.orderedBy}</dd>`
        orderHTML += '</dl>'
        orderHTML += '</section>'
    });
    return orderHTML;
}