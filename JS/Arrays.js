// ARRAY
//
// var shapes =["square", "rectangle", "circle"];
//
//TO CALL SPECIFIC VALUES IN THE ARRAY

// shapes[0] --> square
// shapes[1] --> rectangle
// shapes[2] --> circle

// let age = 24
// let name = "Shaun"
//
// let person = [age + name]

// ***************************************
// "use strict";
// Arrays
// - a data structure that holds on ordered list of items
// - a variable that contains a list of values.
/*
{} braces



[] brackets


BASIC SYNTAX
[]  an empty array

['red'] 1 element

['red', 'blue', 'white'] 3 elements

[4, 4.4, -14, 0] an array of numbers, 4 elements

['red', 4, true, -4]

 */

// let myArray = ['black', 12, -144, false];
// console.log(myArray);
// console.log(myArray.length); // 4
// console.log(myArray[0]); // black
// console.log(myArray[4]); // undefined


// ITERATING ARRAY
// - traverse through elements
// let charlie = ['josh', 'jose', 'nick', 'karen', 'stephen'];
// console.log(charlie);
//
// console.log("There are " + charlie.length + " members in the " +
//     "Charlie Cohort");
// LOOP THROUGH AN ARRAY AND CL THE VALUES
// for (var i = 0; i < charlie.length; i++) {
//     // print out all the values for charlie
//     // individually
//     console.log('The person at index ' + i + ' is ' + charlie[i]);
// }



// forEach Loop
/*
Syntax:
nameOfVariable.forEach( function (element, index, array) {
  .. run the code
 });
 */
// ex.
// let ratings = [25, 34, 57, 62, 78, 89, 91];
// ratings.forEach(function (rating) {
//     console.log(rating);
// });



// let pizza = ["cheese", "dough", "sauce", "pineapple", "canadian bacon"];
// pizza.forEach(function (ingredients) {
//     console.log(ingredients);
// });


// CHANGING / MANIPULATING ARRAYS
let fruits = ['banana', 'apple', 'grape', 'strawberry'];
 console.log(fruits);

// PUSH
// adding to the array
// .push = add the end of the array


fruits.push("watermelon");
console.log(fruits);

// unshift = add to the beginning of an array

fruits.unshift("dragon fruit");
console.log(fruits);
console.log(fruits[0]);
fruits.push("cherry", "mango", "pineapple");
console.log(fruits);

// REMOVE FROM ARRAY
// .pop() - remove the last element of an array

fruits.pop();
console.log(fruits);

// .shift() - remove the first element

fruits.shift();
console.log(fruits);

let removedFruit = fruits.shift();
console.log("Fruit Removed: " + removedFruit);

// LOCATING ARRAY ELEMENT(S)

console.log(fruits);
// .indexOf - returns the first occurrence of what you're looking
// for and the index.

let indexElement = fruits.indexOf('watermelon');
console.log(indexElement);

// SLICING

// .slice - copies a portion of the array
// returns a new array (does not modify the original)
let slice = fruits.slice(2, 5);
console.log(slice);
console.log(fruits);

// SPLICING
// splice removes portion of the actual array
// splice takes two values the starting value and how many elements you want to delete
let splice = fruits.splice(2, 1)

// REVERSE


// .reverse - will reverse the original array
// AND returns the reversed array
fruits.reverse();
console.log(fruits);

// SORT

// .sort - will SORT the original array
// and return the sorted array
console.log(fruits.sort());

// SPLIT

// . split - takes a string and turns it into an array

let codebound = 'leslie,stephen,karen';
let codeboundArray = codebound.split(",");
console.log(codeboundArray);

// JOIN

// .join - takes an array and converts it to a string
// with a delimiter of your choice

let newCodebound = codeboundArray.join(",");
console.log(newCodebound);


