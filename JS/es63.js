'use strict';
const companies = [
    {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];





// filter all of the company names
let companyNames = companies.filter( (company) => company.name)
console.log(companyNames)

// filter all the company names started before 1990
let companyBefore1990 = companies.filter( (company) =>  company.name && company.start_year < 1990)
console.log(companyBefore1990)

// filter all the retail companies  
let retailCompanies = companies.filter( (type) => type.category === 'Retail')
console.log(retailCompanies)


// find all the technology and financial companies??

let techAndFinancial = companies.filter( (company) => company.category === 'Technology' || company.category === 'Financial' )
console.log(techAndFinancial)


// filter all the companies that are located in the state of NY
let allNyState = companies.filter(  (nys) => nys.location.state === 'NY')
console.log (allNyState)

//XXXXXXXX find all the companies that started on an even year.XXXXXXXXXXXX
const allEven = companies.filter( (even) => even.start_year % 2 === 0)
console.log(allEven, "allEven")


// use map and multiply each start year by 2

const timesTwo = companies.map ( (two) => two.start_year * 2)
console.log(timesTwo, "timesTwo")

// find the total of all the start year combined.

const totalYears = companies.reduce( (acc, company ) => acc + company.start_year, 0)
console.log(totalYears)




const companyNamesSorted = companyNames.sort()
console.log(companyNamesSorted)



// display all the company names for youngest to oldest.
