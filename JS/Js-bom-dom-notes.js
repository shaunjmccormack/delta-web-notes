"use strict";
// BOM NOTES
// Browser Object Model
/*

Hierarchy of objects in the browser
We can target and manipulate HTML elements with JavaScript

// Location object
- manipulate the location of a document
    - get query string parameter(s)
Navigator object
- query the info and capabilities of the browser
Screen object
- gets info and manage the web browser's screen
History
- get info/manage the web browser's history
// Window
// Document Object Model (DOM)
 */
// WINDOW OBJECT
/*

represents the JavaScript global object
- all variables and functions declared globally with the 'var' keyword
become the properties of the window object
alert()
confirm()
prompt()
setTimeout()
//
- sets a timer and executes a callback function once the timer expires
 */

/*
setInterval()
- executes a callback function REPEATEDLY with a fixed delay
between each call
 */

// setTimeout()
// Basic Syntax:
// var timeoutID = setTimeout( CallBackFunction{, delay, arg1, arg2... });

// delay is in milliseconds
// delay will default to 0
// function timeoutEx() {
//
//     setTimeout(function () {
//         // alert("Hello Charlie Cohort and welcome to CodeBound!")
//
//         window.location = "https://www.yahoo.com";
//     }, 5000);
//
// }
//
//
// // call my timeoutExt()
// timeoutEx();

// setInterval()

// setInterval( CallBackFunction(), delay, arg1, arg2, ...)

// function intervalEx() {
//
//     setInterval(function () {
//         alert("Hello World!")
//     }, 3000)
//
// }
//
// intervalEx();
// clearInterval(); // allows us to stop the setInterval();
// var count = 0;
// var max = 10;
// var interval = 10000;
//
// var intervalID = setInterval(function () {
//
// //    conditionals
//     if (count >= max) {
//         clearInterval(intervalID);
//         console.log("Finished")
//     } else {
//         count++;
//         console.log(count);
//     }
//
// }, interval);



// DOM - Document Object Model
// manipulate HTML using JavaScript

// Locating elements:
// target them by :
// Element (tag) ex <h1>
// Class     .
// ID        #

// getElementBy //
// BASIC SYNTAX
// document.getElementsBy('Name of Element / Class / Id')
// target id= 'btn3'
//
// var btn3clicked = document.getElementById('btn3');
// alert(btn3clicked);

// ACCESSING FORM INPUTS
// we can access forms using the form collection
// var usernameInput = document.forms.login.username;
// console.log(usernameInput);

// ACCESSING HTML ELEMENTS USING CLASS
// var cards = document.getElementsByClassName('card')
// console.log(cards);

// ACCESSING HTML ELEMENTS USING TAG
// var sections = document.getElementsByTagName('section');
// console.log(sections);

// querySelector()
// returns the first element within that document that MATCHES the specified selector
// or group of selectors

var headerTitle = document.querySelector('header h1');

var mainTitle = document.querySelector('#main-title');

var cardSelector = document.querySelectorAll('.card');

// console.log(cardSelector);
// console.log(headerTitle);
// console.log(mainTitle);

// ACCESSING / MODIFYING ELEMENTS AND PROPERTIES

// get the value of the innerHTML
//
// var title = document.getElementById('main-title');
// console.log(title); // get the structure of #main-title
// //
// // only want the content of the element?
//
// console.log(title.innerHTML);
//
// // or
// console.log(document.getElementById('main-title').innerHTML);
//
set the value of the innerHTML

title.innerHTML = "Hello <em>Charlie Cohort</em>!";

document.getElementById('main-title').innerHTML = '<h1>Hello World!</h1>';

// ACCESSING AND MODIFY using attributes
// check if the attribute exists

var checkForm = document.forms['login'];




// console.log(checkForm);
// console.log(checkForm.hasAttribute('action'));
// true
// get an attribute value
// console.log(checkForm.getAttribute("method"));
// create a new attr. or change a value of an existing


</div>
</div>
</div>







//checkForm.setAttribute('id', 'feedback-form');

// adds a id name

checkForm.setAttribute('method', 'GET');

// changes the method from POST to GET
// console.log(checkForm);
// delete attr.

checkForm.removeAttribute('id');

// console.log(checkForm);
// Accessing and modifying styles
// single style

var jumbotron = document.querySelector('.jumbotron');

// jumbotron.style.display = 'none';
/*

same as in CSS...
.jumbotron {
    display: none
}
 */

jumbotron.style.fontFamily = "Comic Sans Ms";

// multiple styles

Object.assign(jumbotron.style, {
    border: "10px solid blue",
    fontFamily: "Trajan",
    textDecoration: "underline"
});

// styling node list
var tableRows = document.getElementsByTagName("tr");

// for (var i = 0; i < tableRows.length; i++) {
//
//     if (i % 2 !== 0 && i !== 0) {
//         tableRows[i].style.background = "skyblue";
//     }
//
// }

// first: convert tr into an array

var tableRowArray = Array.from(tableRows);
// now we can use a forEach

tableRowArray.forEach(function (tableRow) {
    tableRow.style.background = 'skyblue';
});

// adding and removing elements
// createElement()
// removeChild()
// appendChild()
// replaceChild()
// what is the name(s) of the parameters?
// what is the function doing (returning)?
// function timeoutEx() {
//
// setTimeout(function () {
//     window.location = 'https://www.google.com'
// },8000);
// }
//
// timeoutEx();










