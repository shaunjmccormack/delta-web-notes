/* JAVASCRIPT DOM */
// NOTE: You'll need to create an html to link this js file, along with creating some content in the html

"use strict";

/** When the button with the id of `change-bg-color` is clicked the background of the page should turn blue.*/

const button = document.getElementById("change-bg-color");
button.addEventListener('click',function (){
    document.body.style.background = "blue";
})


/** When the button with an id of `append-to-ul` is clicked, append an li with
 the content of `text` to the ul with the id of `append-to-me`.*/

const button1 = document.getElementById("append-to-ul");
button1.addEventListener('click',function (){
    document.getElementById("append-to-me").innerHTML =
   ("<li>text</li><li>text2</li><li><h2>Hello</h2></li>")
})
let ul = document.getElementById("append-to-me");
ul.innerText = "<li> h2 </li>";


/** Two seconds after the page loads, the heading with the id of `message` should
 change it's text to "Goodbye, World!". Completed*/
setTimeout(function(){
    document.getElementById("message").innerHTML =
    "Goodbye, World!"
},2000)


const heading = document.createElement('h1')
heading.innerHTML = "Hello, World";
heading.id = "message";


document.body.append(heading);


/** When a list item inside of the ul with the id of `hl-toggle` is first
 clicked, the background of the li that was clicked should change to
 yellow.


 When a list item that has a yellow background is clicked, the
 background should change back to the original background.
 */

const button3 = document.getElementById("append-to-me");



/** When the button with the id of `upcase-name` is clicked, the element with the
 id of `output` should display the text "Your name uppercased is: " + the
 value of the `input` element with the id of `input` transformed touppercase.*/



/** Whenever a list item inside of the ul with the id of `font-grow` is _double_

 clicked, the font size of the list item that was clicked should double.*/


"use strict";
