/* ES6 DRILLS */
// Complete the following in a new js file named 'es6-drills.js'
const petName = 'Chula';
const age = 14;


// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + petName +
//     ' and she is ' + age + ' years old.');

console.log (`My dog name is ${petName} and she is ${age} years old`)

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }
const addTen = num => num + 10;


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }
const minusFive = num1 => num1 -5;

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }
const timesTwo = num2 => num2 * 2;

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
const greetings = function (name) {
    return "Hello, " + name + ' how are you?';
};

const greetings = name => `Hello ${name} how are you` 

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }


// example-1
const haveWeMet = name => (name === 'bob') ? `${name} Nice to see you again` : "Nice to meet you"; 
// example-2
const haveWeMet = name => if (name === 'bob') {return `${name} Nice to see you again`} else {"Nice to meet you"}};