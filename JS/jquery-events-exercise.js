/* jQuery Events
// Complete the following in a new js file named jquery-events-exercise.js
// NOTE: You'll also name a new html file, name this jquery-events-exercise.html
*/
// Add jQuery code that will change the background color of a 'h1' element when clicked.
$('h1').click(function() {
    $(this).css('background-color', 'red')
})
// Make all paragraphs have a font size of 18px when they are double clicked.
$('p').dblclick(function() {
    $(this).css('font-size','18px')
})
// Set all 'li' text color to green when the mouse is hovering,
// reset to black when it is not.
$('ul').hover(function (){$(this).css('color','green')
    },
    function (){$(this).css('color','black')}
)
// Add an alert that reads "Keydown event triggered!" everytime a user pushes a key down
$('body').keydown(function (){
    alert("hey!!no stop dont!!")
})
// Console log "Keyup event triggered!" whenever a user releases a key.
$('body').keyup(function (){
    console.log("hey! get back here! no dont stop!!!")
})
// BONUS: Create a form with one input with a type set to text.
// Using jQuery, create a counter for everytime a key is pressed.

