
    // "use strict";
    // CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY
    // Exercise 1:

    // Write a function name 'wait' that accepts a number as a parameter, and returns

    //a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
        // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));


function wait(number){
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
            resolve('you will see this is 4 seconds')
        },4000)
    });
}
                 //or


const wait = (number) => {
    return new Promise( (resolve, reject) =>{
        setTimeout( () => {
            resolve()
        },number)
    })
}
wait(4000).then( () => console.log(data))



        // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));
      function wait(number){
            return new Promise( (resolve, reject) => {
                setTimeout( () => {
                    resolve('you will see this is 4 seconds')
                },8000)
            });
        }

    // Exercise 2:
    // Write a function testNum that takes a number as an argument and returns a Promise that tests if the value is less than or greater than the value 10.

const TestNum = (num) => {
    return new Promise ( (resolve, reject) => {
        if (num < 10) {
            resolve('less than 10')
        } else {
            reject('greater than ten')
        }
    })
}
TestNum(20).then( (data) => console.log(data)).catch( (error) => console.log(error));
    //// Exercise 3:
       // //Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order. If the array contains anything but strings, it should throw an error.

 



const makeAllCaps = (words) => {
    return new Promise( (resolve, reject) => {
        let capitalizedWords = words.map( (word) => {
            if (typeof word === 'string'){
                return word.toUpperCase();
            } else {
                reject ('Error: Not all items in the array are strings')
            }
        });
        resolve(capitalizedWords) 
    });
}

const sortWords = (words) => {
    return new Promise( (resolve, reject ) => {

        //let wordsArr = words.filter( (word) =>typeof word === 'string');

        words.forEach(word => { 
            if (typeof word !== 'string') {
                reject('this is not a string!')
            }
        });
        resolve(words.sort())
     });
}