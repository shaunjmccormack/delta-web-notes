"use strict";
// "use strict";


// JAVASCRIPT LOOPS

// allows us to execute code repeatedly
// while loop, do-while loop, for loop, and break and continue


// while loop
// is a basic looping construct that will execute
// a block of code, as long as a certain
// condition is true
/*
syntax:
while (condition) {
    // run the code
}
 */

}


// do-while loop
/*
the only difference from a while loop,
is that the condition is evaluated
at the end of the loop.
Instead of the beginning
 */
/* syntax
do {
/// run the code
} while (condition)
 */
// ex.
// var i = 10;
// do {
//     console.log("Do-while #" + i);
//     // decrement by i
//     // i -= 1;
//     i--;
// } while (i >=0);


// for loop
/*
is a robust looping mechanism
available in many programming
 */
// syntax
/*
for (initialization; condition; increment/decrement) {
    // run the code
}
 */
// example.
// for (var x = 100; x <= 200; x+=10) {
//     console.log("For loop #" + x);
// }


// BREAK AND CONTINUE
/*
- breaking out of a loop
- using the 'break' keyword allows us to exit the loop
 */
var endAtNumber = 5;

for (var i = 1; i <= 100; i++) {

    console.log("Loop count #" + i);

    // if statement
    if (i === endAtNumber) {
        alert("We have reached our limit. Break!");
        break;
        console.log("Do You See This Message???");
    //    ^ you will never see this console.log message...
    }
}obj

// continue keyword
// - continues the next iteration
// of a loop
for (var n = 1; n <= 100; n++) {
    // if statement
    if (n % 2 !== 0) {
        continue;
    }
    console.log("Even Number: " + n);
    // prints out n % 2 === 0
}
