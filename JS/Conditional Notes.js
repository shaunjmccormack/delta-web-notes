"use strict";
// enable strict mode
// with strict mode, you cannot use undeclared variables

// CONDITIONAL STATEMENTS

// if statements
//     - allow us to execute code based on certain conditions

// SYNTAX for if statements
// if ( condition ) {
    // if the condition is true execute this code
// }

// example
let numberOfLives = 1
if (numberOfLives === 0){
    console.log("Game Over!")
}

// if / else statements
// else statement will execute if all above conditions are false

// SYNTAX for if / else Statements
// if (condition){
//     code will be executed if true
// } else {
//     code will be executed
// }
let name = "Shaun";

if (name === "John") {
    console.log("Your name is not John");
}

// SYNTAX for else / if Statements
// if (condition){
//     code will be executed if true

// } else if ( another condition) {
//     code will be executed

// }  else  {
//      code will be executed
//  }

if (name === "Jeffrey"){
    console.log("Your name is Jeffrey")
} else if (name === "Justin"){
    console.log("Your name is Justin")
} else {
    console.log("I dont know your name bro!")
}

// SWITCH STATEMENTS
// another method for writing conditional statements
// increases readability for chained if statements

//SYNTAX for Switch Statements
/*
    switch ( condition ) {
    case // code gets executed
    break // stops the code from running

            case ""
            break;

    default // equivalent to the else statement
    }
 */

//example
let color = "Teal";
switch (color) {
    case "Yellow":
        console.log("You chose Yellow")
        break;
    case "Blue":
        console.log("You chose Blue")
        break;
    case "Magenta":
        console.log("You chose Magenta")
        break;
    Default:
        console.log("You didnt chose the right color")
}

// add if statement screen shot



//create a switch statement for weather
// sunny, cloudy, rain, snow, default
let currentWeather "Hail"

switch (currentWeather) {
    case "Sunny":
        console.log("You chose Sunny")
        break;
    case "Cloudy":
        console.log("You chose Cloudy")
        break;
    case "Rain":
        console.log("You chose Rain")
        break;
    case "Snow":
        console.log("You chose Snow")
        break
    default:
        console.log("Hail")
}

//  TERMINAL OPERATORS
// shorthand way of writing if/else statements
// only used when there are only two choices


// let name = "Shaun";
//
// if (name === "John") {
//     console.log("Your name is not John");
// } else {
//     console.log("Your name is not John")
// }

( name === "John") ? console.log("Your name is John") : console.log("Your name is not John");

let burgerPreference = prompt("What kind of burger do you like?");

if (burgerPreference === "Bacon and Cheese") {
    alert("Great Choice!")
} else if (burgerPreference === "Cheese"{
    alert("noice")
} else if (burgerPreference === "Chilly"){
    alert ("ok...")
} else if (burgerPreference === "Vegan"){
    alert ("Get out!!!")
} else {
    alert("boring")
}



