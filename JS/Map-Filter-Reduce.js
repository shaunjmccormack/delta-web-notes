// MAP FILTER REDUCE EXERCISE
// Complete the following in a new js file name 'map-filter-reduce-exercise.js'

// an arroay of objects called developers

const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];
/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/
let moreThanFiveLanguages = developers.filter((developer) => {developer. languages.count >5)

 // let language = languages.filter((morethan5) => morethan5 > [4] );
// return morethan5

/**Use .map to create an array of strings where each element is a developer's
 email address*/

// let emailAddresses = developers.map ((developer) => developer.email)


let mapOfEmails = developers.map(function (emails){
    return emails.email
}); 
console.log(mapOfEmails)

/**Use reduce to get the total years of experience from the list of developers.
 * Once you get the total of years you can use the result to calculate the average.*/
let totalYearsExperience = developers.reduce((acumulator, developer) =>{
    acumulator + developer.yearsExperience
});


let totalYearsExperience = developers.reduce(function (acumulator, developer){
    return acumulator + developer.yearExperience
}, 0);
console.log(totalYearsExperience)

let average = totalYearsExperience / developers.length
console.log(average)



/**Use reduce to get the longest email from the list.*/

// function findLongestEmail(str){
    // let longestEmail = developers.reduce(function(longest,developer){
    //     return developer.email.length > longest.length ? longest = developer.email : false
        
    //     //regular if statement example
    //     // if (developer.email.length > longest.length) {longest = developer.email} 
    // },"");

    // console.log(longestEmail)
      
    

    const longestEmail = developers.reduce((acumulator, developer) => acumulator.email.length > developer.email.length ? acumulator : developer).email;

// };

/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, leslie, dwight*/
// let namesOfAll = developers.join(function())

let devNames = developers.reduce((accumulator, developer) => names + developer.name, "codebound Names")

// BONUS
/** Use reduce to get the unique list of languages from the list
 of developers
 const albums = [
    {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}];
/** Create a filter function that logs every 'Pop' album*/
let popAlbums = albums.filter( (album) => album.genre === pop)


/**  Create a filter function that logs every 'Rock' album*/

let rockAlbums = albums.filter( (album) => album.genre === rock)

/** Get the total value of years */

let totalYears = albums.reduce( (acc, album ) => acc + album.released, 0)