"use strict";
// OBJECTS
/*
Object is a grouping of data and functionality.
Data items inside of an object = PROPERTIES
Functions inside of an object = METHODS


CUSTOM OBJECTS
Prototypes that allows existing object to be
used as templates to create new objects


Object() keyword - the starting point to make
custom objects

 */


// NEW OBJECT INSTANCE
// var car = new Object();
// console.log(typeof car);

// The use of 'new Object()' calls the Object
// CONSTRUCTOR to build a new INSTANCE of object.


// OBJECT LITERAL NOTATION
// - curly braces {}

//var car = {};

// alert(typeof car); // object


// SETTING UP PROPERTIES ON A CUSTOM OBJECT
// dot notation
// car.make = 'Toyota';
// // car.color =;
// // car.year
// this is creating a car object
// let car = {};
// cars.color = "Black";
// car.year = "2020";
// car.model = "Model S";
// car.make = "Telsa";
// console.log(car)
//
// let plane = {};
// plane.wing_count = 2;
// plane.make = "Boeing";
// plane.wheel_count = 48;
// console.log(plane)


//
// // array notation
// car.model = '4Runner'
// car['model'] = '4Runner';
//
// console.log(car);

// MOST COMMON WAY TO ASSIGN PROPERTIES
// let car = {
//     make: 'Toyota',
//     model: '4Runner',
//     color: 'Hunter Green',
//     numberOfWheels: 4
// };
//
// console.log(car);
//
// console.log(car.model);
//
//
// // ADDING ON MORE PROPERTIES
// // DON'T DO THIS
// // car["numberOfDoors"] = 5;
//
// // INSTEAD
// car.numberOfDoors = 5;
// console.log(car);


// NESTED VALUES

var cars = [
    {
        make: 'Toyota',
        model: '4Runner',
        color: 'Hunter Green',
        numberOfWheels: 5,
        features: ["Heated Seats", "Bluetooth Radio", "Automatic Doors"],
        alarm: function () {
            alert("Sound the alarm!!");
        }
    },
    {
        make: 'Honda',
        model: 'Civic',
        color: 'Black',
        numberOfWheels: 4,
        features: ["Great Gas Mileage", "FM/AM Radio", "Still Runs"],
        alarm: function () {
            alert("No alarm... sorry");
        },
        owner: {
            name: 'John Doe',
            age: 35
        }
    },
    {
        make: 'Nissan',
        model: 'Sentra',
        color: 'Red',
        numberOfWheels: 4,
        features: ["Great Gas Mileage", "FM/AM Radio", "Still Runs", "Powered Windows", "GPS Navigation"],
        alarm: function () {
            alert("Call the cops!");
        },
        owner: {
            name: 'Jane Doe',
            age: 30,
            address: {
                street: '150 Alamo Plaza',
                city: 'San Antonio',
                state: 'TX',
                citySlogan: function () {
                    alert("Go Spurs Go!")
                }
            }
        }
    }
];

// console.log(cars);

// console.log(cars);

// get the honda object

// console.log(cars[1]);

// get the model of the honda object only

// console.log(cars[1].model);

// get the owner of the nissan sentra only

// console.log(cars[2].owner);

// get the city of the nissan's owner

// console.log(cars[2].owner.address.city);

// get the second feature of the honda civic only

// console.log(cars[1].features[1]);

// get the alarm function for the first object
//cars[0].alarm();

// display the features of
// all cars

// cars.forEach(function (car) {
//     car.features.forEach(function
//         (feature) {
//         console.log(feature);
//     });
//});


//'this' keyword

/*

this - is simply a reference to the CURRENT object
- can refer to a different object based on how a function
is called.
 */

// ex.
//
// var videoGame = {};
// videoGame.name = 'Madden 20';
// videoGame.company = 'EA Sports';
// videoGame.genre = "Sports";
// // create a method on the videoGame
// // object
// videoGame.description =
//     function () {
//         alert("Video Game: " + this.name
//             + " \nCompany of Video Game: " + this.company
//             + " \nGenre of Video Game: " + this.genre);
//     };
// videoGame.description();
