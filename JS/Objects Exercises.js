// let person = {
//     firstName : 'Shaun',
//     LastName : 'McCormack',
//     Age : 41,
//     Hobbies : ["SnowBoarding", "Hiking", "Music"]
// }
// console.log(person)
//
// person.stuff = ["things", "stuff", "moneys"]
//
// console.log(person)




/* OBJECTS, LOOPS, FUNCTIONS... OH MY! */
// Complete the following in a new js file named objects-loops-functions.js



// "use strict";

const albums = [
    {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
];

// for (let i = 0  ; i < albums.length; i++) {
//     // if (albums[i].genre) {
//     //     console.log(albums[1].genre)
//     if (albums[i].genre === 'Pop'){
//         console.log(albums[i])
//     }
// }
//

// albums.forEach(function (album) {
//     if (album.genre === 'Rock') {
//         console.log(album);
//     }
// });

// albums.forEach(function(album){
//     if (album.released <= 2000) {
//         console.log(album)
//     }
// })

// albums.forEach(function (between){
//     if (albums.released >= 1900 && albums.released <= 2020) {
//         console.log(between);
//     }
// })

for (let i = 0; i < albums.length; i=== 0){
    if (albums[i].released > 1900 && albums[i].released < 2020){
        console.log(albums[i]);
    }
}

// albums.forEach(function (artists) {
//     if (artists.artist === "Michael Jackson") {
//         console.log(artists);
//     }
// });
// let addAlbum = function (artists, genre, released, title) {
//     let album = {
//         artists: artists,
//         genre: genre,
//         released: released,
//         title: title
//     }
//     albums.push(album)
// }
// addAlbum("Justin", "RnB", 2021, "Soul")
// console.log(albums)

/**
 * 1.xxx create a for loop function that logs every 'Pop' album

 * 2. create a for each function that logs every 'Rock' album

 * 3. create a for each function that logs every album released before 2000

 * 4. create a for loop function that logs every album between 1990 - 2020

 * 5. for loop function that logs every Michael Jackson album

 * 6. create a function name 'addAlbum' that accepts the same parameters from 'albums' and add it to the array
 */
/**
 *
 *
 * TODO: Write a program that prints the numbers from 1 to 100.
 *  But for multiples of three print "Fizz" instead of the number
 *  and for the multiples of five print "Buzz".
 *  For numbers which are multiples of both three and five print "FizzBuzz".
 */


/**
 * TODO: Write a function that takes in two parameters
 *  and returns the sum of both parameters, then multiplies it by 5.
 */
/* Create a function that prompts a user for their favorite day of the week and alerts a unique message based on the day.
Catch any invalid inputs (not indicating a day of the week).
For each day, allow the user to enter the abbreviated day (e.g. 'Monday', 'monday', 'Mon', or 'mon')
*/
/* Create a function that will return how many whitespace characters are at the beginning and end of a string. */
