"use strict";
// EVENT LISTENERS
//  addEventListener()

// BASIC SYNTAX

// target.addEventListener( type, listener, [useCapture]);

// Target = is the object the event listener is registered on.

// Type - type of event that is being listened for
// Listener - the function that is called when an event
// of type happens on the target.

// UseCapture - a boolean that decides if the
// event-capturing is used for event triggering.

// TYPE OF EVENTS

// keyup ( key is released )
// keydown ( key is pressed down )
// click ( mouse is clicked )
// change ( input gets modified )
// submit ( when form is submitted )
// mouseover


var testButton = document.getElementById('testBtn');


// ADD EVENT LISTENER USING AN ANONYMOUS FUNCTION


testButton.addEventListener("click", function () {
//
    if (document.body.style.background === 'red') {
        document.body.style.background = 'white';

    } else {
        document.body.style.background = 'red';
    }

});

//add event listener from a previously defined function
// function toggleBackgroundColor() {
//     if (document.body.style.background === 'red') {
//         document.body.style.background = 'white';
//         testButton.removeEventListener('click', toggleBackgroundColor)
//     } else {
//         document.body.style.background = 'red';
//     }
// }
//
// testButton.addEventListener('click', toggleBackgroundColor);


// REGISTER ADDITIONAL EVENTS
// cursor hovers over a paragraph, change the color of the text, font-family
// and make font larger


var paragraph = document.querySelector("p");


// var paragraph = document.getElementsByTagName( "p")[0];
// change font color, font-family, and size


function makeColorChange() {
    paragraph.style.color = "green";
    paragraph.style.fontFamily = "Comic Sans Ms";
    paragraph.style.fontSize = "30px";
}


// add a mouse over event to the 'p' that will trigger the makeColorChange()
// function

paragraph.addEventListener("mouseover", function () {
    makeColorChange();
});
