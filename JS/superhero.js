$('#prev').click(function () { 
    console.log('last'); 
    let currentElement = $('.visible')
    let previousElement = currentElement.prev()

    currentElement.removeClass('visible')
    currentElement.addClass('visually-hidden')
    previousElement.removeClass('visually-hidden')
    previousElement.addClass('visible')

    if(previousElement.html() == undefined) {
        console.log(here)
      let lastElem = $("ul li").last()
      lastElem.removeClass('visually-hidden')
      lastElem.addClass('visible')
    }

});

// get the previous button element from html
// then add a click event to that element
$('#prev').click(function() {
    // logged previous to see if the click event is happening
    console.log('previous');
    // get the current element
    let currentElement = $('.visible')
    // get the previous
    let previousElement = currentElement.prev()
    //switch classes for visibility
    currentElement.removeClass('visible')
    currentElement.addClass('visually-hidden')
    previousElement.removeClass('visually-hidden')
    previousElement.addClass('visible')
    // if the previous element is not defined
    if (previousElement.html() == undefined) {
        // log that it is not defined
        console.log("undefined prev element")
        // get the last element and display it
        let lastElem = $( "ul li" ).last()
        lastElem.removeClass('visually-hidden')
        lastElem.addClass('visible')
    }
});



// $('#next').click(function () { 
//     console.log('next');
//     let currentElement = $('.visible')
//     let nextElement = currentElement.next()

//     currentElement.removeClass('visible')
//     currentElement.addClass('visually-hidden')
//     nextElement.removeClass('visually-hidden')
//     nextElement.addClass('visible')

//     if(nextElement.html() == undefined) {
//         console.log('next')
//       let fistElem = $("ul li").first()
//       firstElem.removeClass('visually-hidden')
//       firstElem.addClass('visible')
//     }

// });
//get the next button then add an event to happen on click of this element
$('#next').click(function() {
    console.log('next');
    //get the current Elem
    let currentElement = $('.visible')
    //get the next Elem
    let nextElement = currentElement.next()
    // toggle the visibility by switching the classes
    currentElement.removeClass('visible')
    currentElement.addClass('visually-hidden')
    nextElement.removeClass('visually-hidden')
    nextElement.addClass('visible')
    //if the element is undefined go to the first elem and make it visible
    if (nextElement.html() == undefined) {
        console.log("here")
        let firstElem = $( "ul li" ).first()
        firstElem.removeClass('visually-hidden')
        firstElem.addClass('visible')
    }
});


