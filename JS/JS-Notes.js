// EXTERNAL JAVASCRIPT
//
// // single line comment
//
// /*
// this
// is
// a multi
// line
// comment
//  */
//
//
// Console log
// console.log(5+10)
//
// // DATA TYPES
//
// //Primitive Types
// // number, string, boolean, undefined, null
//
// //VARIABLES
// //var, let, const
//
// // DECLARE VARIABLES SYNTAX
// // var nameOfVariable
// // let nameOfVariable
// // const nameOfVariable
//
// //EXAMPLE
// var number = 25;
// console.log(number)
//
// var firstName;
// firstName= "Shaun"
// console.log(firstName)
//
// const messsage = "Alert"
// alert(alert)
//
// const answer = prompt("What is your favorite color?");
// alert(answer);


// //OPERATORS
// //ARITHMETIC
//
// //addition, subtraction, multiplication, division, modulus (retrieves the remainder)
let remainder = 5 % 4;
console.log(remainder
)
// //PEMDAS
// console.log((1=2) * 4 / 5)
// //modulus example
// console.log( 5 % 4)
//
// //LOGICAL OPERATORS
//
// // && AND
// // || OR
// // !  NOT
//
// //returns a boolean value ( TRUE or FALSE)
//
// // AND or && true  true
// // true && true    true
// // true && false   false
// // false && false  true
// // false && true   false
//
// OR operator
// // true || false  true
// // true || true   true
// // false || false  false
// // false || true  true
//
// // NOT or ! operator
// // !true false
// // !false true
//
// // !!!!!!!!true
//
// // assigns value
// // =
// var variable = 1
//
//
// // compares value
// // ==
// // compares types and value
//
// // console.log(5 === '5'); --> false
//
// let firstPet = "Dog";
// let secondPet ="dog";
//
// console.log(firstPet === secondPet)
//
// let age = 23;
// let ageTwo = "23";
// console.log(age == ageTwo); --> true
// console.log(age === ageTwo); -->false

let firstname  = "shaun"
let lastname   = "mccormack"
let id = 1234
let height = 9000
console.log(firstname !== lastname && 43 <= id
    ||height !== id && id !== 1234)


// != check if the value is NOT equal
// !== check if value AND data type are not equal
//
//
// //greater than, less than, less than or equal to, greater than or equal to
// // >, <, <=, >=
// const age = 21
//
// console.log(age >= 21 || age < 18) --> true
// console.log(age <= 21 || age > 18 ) --> true
// console.log(age < 21 &&  age > 18) --> false


// CONCATENATION



// const  person = "Bruce"
// const person_age = 34;
//
// console.log( "Hello my name is" + person + "and i am" + person_age)
//
// const message = "hello my name is " + person + "and I am" + person_age
//
// console.log(message)
//
// // template string
//
// console.log(`Hello my name is ${person} and I am ${person_age}`);
//  string interpolation ^

//  // Get the length of the string
// console.log(person.length); --> 5

// const greeting = "Hello World!"
// console.log(greeting.length)
//
// // retrieve the length of a string
// console.log(greeting.length)
//
// UPPERCASE and LOWER UPPERCASE
//
// console.log(greeting.toLocaleUpperCase())
// console.log(greeting.toLocaleLowerCase())

// const message = "Alert!"
// // alert(message)
// alert(message)
// // prompt(question)


// const message = "Alert!"
// // alert(message)
// alert(message)
// let answer = (message)
// console.log(answer)



// if true run this
if (3 > 2) {
    // run this code
}else if (3 < 2){
    // run this if first condition is false
} else {
    // in every other scenario run this
}

let time = 1800;
let greeting;

if (time > 2000){
    greeting = "Hello!";
} else if (time < 2000){
    greeting = "Goodbye!";
} else if (time === '1800'){
    greeting = "lkjsdf;ajf";
}else {
    greeting ="Suuuuuuuup";
}

console.log(greeting);

let car1= "Tesla"
let car2= "Porsche"
let car3= "Ford"

let cars =[car1, car2, car3]
console.log(cars[0])

// let cars = new Array(car1, car2, car3)

// FUNCTIONS

function naneOfTheFunction (parameter1, parameter2) {
    //what we want to do
}

function add ( num1, num2) {
    console.log(num1 + num2);
}

add( 2, 5);

function subtract (num1, num2) {
    console.log(num1 - num2);
}
subtract( 10, 10000)

// Anonymous function
let alertsUser = function () {
    alert("Yooooooooo user!");
}
function doThis ( num1, param2, num3, num4){
    console.log(add(num1, param2))
    subtract(num3, num4)
    alertsUser();
}
doThis( 20, 4321, 5432, 654365)
