"use strict";
// Document Ready

// window.onload = function () {
//     alert("This page has finished loading")
// };
// jQuery Object
/*
- object used to find and create HTML elements from the DOM
 */

// syntax:
/*
$(document).ready(function() {
    // run the code...
});
 */
// jQuery Document Ready
// $(document).ready(function () {
//     alert("This page has finished loading")
// });

// In jQuery, we use the dollar sign $ to reference the jQuery object
// $ is an alias of jQuery

// jQuery Selectors
// ID, class, element, multiple, all
/*


#
.
elementTagName
selector1, selector2, selector3, ...
*
 */
// syntax for jquery selectors
// $("selector")

// getting an element by id
// w/vanilla js
// document.getElementById("type")
// w/jQuery
// $("#type")

// getting an element by class
// document.getElementsByClassName("urgent")
// w/jQuery
// $("#urgent")
//

// .html  - returns the html content(s) of the selected element(s)
// SIMILAR to '.innerHTML' property
// .css   - allows us to change css properties for the selected element(s)
// SIMILAR to the '.style' property
// example
// ID SELECTOR
// var content = $('#codebound').html();
// alert(content);

// CSS SELECTOR

$('.urgent').css('background-color', 'red'); // single property
$('.not-urgent').css(
    {
        'background-color': 'yellow',
        'text-decoration': 'line-through'
    }
); // multiple properties
/*
same as if in an css...
.urgent {
    background-color: 'red';
 }
 */
// MULTIPLE SELECTOR
$('.urgent, p').css('color', 'orange');
$('.urgent, p').css('text-decoration', 'underline');
// all selector
// $('*')
// element selector
$('h3').css('border-style', 'dotted');












